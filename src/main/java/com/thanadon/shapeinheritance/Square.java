/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.shapeinheritance;

/**
 *
 * @author Acer
 */
public class Square extends Rectangle {

    public Square(double side) {
        super(side, side);
        System.out.println("Square Created");
    }
    
    @Override
    public void print() {
        System.out.println("Square: Side = " + this.width + ", Area = " + this.calArea());
    }
}
