/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.shapeinheritance;

/**
 *
 * @author Acer
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape();
        shape.print();
        
        Circle circle = new Circle(4);
        circle.print();
        
        Triangle triangle = new Triangle(4,3);
        triangle.print();
        
        Rectangle rectangle = new Rectangle(6,5);
        rectangle.print();
        
        Square square = new Square(5);
        square.print();
        
        System.out.println();
        System.out.println("shape is Shape: "+ (shape instanceof Shape));
        System.out.println("shape is Circle: "+ (shape instanceof Circle));
        
        System.out.println("circle is Shape: "+ (circle instanceof Shape) );
        System.out.println("circle is Circle: "+ (circle instanceof Circle) );
        System.out.println("circle is Object: "+ (circle instanceof Object) );
        
        System.out.println("square is Rectangle: "+ (square instanceof Rectangle));
        System.out.println("square is Shape: "+ (square instanceof Shape));
        
        System.out.println();
        Shape[] shapes = {circle, triangle, rectangle, square};
        for(int i = 0; i < shapes.length; i++){
            shapes[i].print();
        }
    }
        
}
