/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.shapeinheritance;

/**
 *
 * @author Acer
 */
public class Triangle extends Shape {

    private double base;
    private double height;

    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
        System.out.println("Triangle Created");
    }

    @Override
    public double calArea() {
        return (base * height) / 2.0;
    }

    @Override
    public void print() {
        System.out.println("Triangle: Base = " + this.base + ", Height = " + this.height
                + ", Area = " + this.calArea());
    }
}
