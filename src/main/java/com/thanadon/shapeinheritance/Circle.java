/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.shapeinheritance;

/**
 *
 * @author Acer
 */
public class Circle extends Shape {

    private double r;
    private double pi = 22.0 / 7.0;

    public Circle(double r) {
        this.r = r;
        System.out.println("Circle Created");
                
    }

    @Override
    public double calArea() {
        return pi * r * r;
    }

    @Override
    public void print() {
        System.out.println("Circle: R = " + this.r + ", Area = " + this.calArea());
    }
}
